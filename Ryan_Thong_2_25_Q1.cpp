//Date: 2/25/2020
//Author: Ryan Thong
//This file is recreating the Fibonacci Sequence for integers smaller than 4 million. In addition, it is searching for all of the even numbers in the sequence and summing it together.
#include <cmath>
#include <iostream>
#include <vector>

using namespace std;

int main(){

    vector<int> Fib_Seq_Array;//creating the vector
    
    Fib_Seq_Array.push_back(1);//intializing the vector
    Fib_Seq_Array.push_back(2);//intializing the vector
    
    int maxNum = 4000000;//the upper limit on the while loop
    int counter = 1;//the variable to continue the while loop
    
    int Fib_Sum = 2;//initializing the even sum
    
    while (Fib_Seq_Array[ Fib_Seq_Array.size() - 1] <  maxNum){//condition for the forloop is to check if the last element in the Fibonacci vector is less than the upper limit
        Fib_Seq_Array.push_back(Fib_Seq_Array[counter] + Fib_Seq_Array[counter-1]);
        
        counter++;
        
        if( Fib_Seq_Array[ Fib_Seq_Array.size() - 1] % 2 == 0)//checking if an integer is even
            Fib_Sum = Fib_Sum + Fib_Seq_Array[ Fib_Seq_Array.size() - 1];
    }
    cout << "The Fibonacci Sum for even numbers are: " << Fib_Sum <<endl;
}
