//Date: 2/25/2020
//Author: Ryan Thong
//This file is finding the 1001st prime number by checking if a number is a factor of any number between 2 and itself. If so, the loop would break out and disregard the number.
//However, if the number was truly prime, then the number would be stored.
//For this instance, a while loop was more efficient because the prime numbers were stored in a vector. Since the vector was continually growing, the size limit was set to 1001
//A for loop would be insufficient because it requires a more definite integer
#include <cmath>
#include <iostream>
#include <vector>

using namespace std;

int main(){
    vector<int> primeNums;//creating the prime number vector
    
    primeNums.push_back(2);//initializing the vector
    
    int num = 3;//initializing the counter since 2 is already in the vector
    bool check = false;//creating a boolean to check if a number is prime
    
    while ( primeNums.size() < 1001 ){
        for( int i=2; i<num; i++){
            if( num%i == 0){//if number was not prime then proceed, othewise do not
                check = true;
                break;
            }
        }
        if(!check)//storing the prime number
            primeNums.push_back(num);
        check = false;
        num++;
    }
    
    cout<< "The 1001st prime number is: "<< primeNums[primeNums.size()-1] <<endl;
}
