//Date: 2/25/2020
//Author: Ryan Thong
//This file is calculating the sum of squares and square of sums of natural numbers through the use of a for loop, and calculating the difference of them
#include <cmath>
#include <iostream>

using namespace std;

int main(){
    
    int sumOfSquares = 0;//initialize to 0 because running rum
    int squareOfSums = 0;//initialize to 0 because running rum
    
    for(int i=1; i<101; i++){
        sumOfSquares = sumOfSquares + pow(i,2);
        squareOfSums = squareOfSums + i;
    }
    squareOfSums = pow(squareOfSums,2);
    
    int difference;
    
    difference = squareOfSums - sumOfSquares;
    
    cout<< "The difference between the sum of squares and the square of sums is: " << difference <<endl;
}
