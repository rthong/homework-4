//////////////////////////////////////////////
//
// Author : Samuel Meehan <samuel.meehan@cern.ch>
//
// Description : This is a toy monte carlo generator to simulate the
// two-body decay of a massive particle with a mass of 15 GeV and a width of 3 GeV
// assuming it is a Gaussian. It will print the event kinematics of the two
// outgoing particles.
//
//////////////////////////////////////////////
#include <iostream>
#include <random> // HINT : You may need to include a header to use random stuff in c++
#include "FourVector.h"// HINT : You may need to include a header to give access to the FourVector class

using namespace std;


// start tools for generating Gaussian random numbers
default_random_engine generator;
normal_distribution<double> gen_mass(15.0,3.0);
uniform_real_distribution<double> gen_esplit(0.2,0.7);
uniform_real_distribution<double> gen_theta(0.0,M_PI);
uniform_real_distribution<double> gen_phi(0.0,2*M_PI);
// function prototype for the generation of a single event kinematics
void GenEvent(FourVector *v1, FourVector *v2);

int main(int argc, char *argv[]){
cout<<"STARTING"<<endl;
if(argc==1){
cout<<"Must give me a number of events - exitting"<<endl;
return 11;
}
int nevents = atoi(argv[1]);
cout<<"Generating NEvents : "<<nevents<<endl;
// define the two four vectors from the decay
FourVector *fv1 = new FourVector();
FourVector *fv2 = new FourVector();

// event loop to generate some number of decays
for(int iEv=0; iEv<nevents; iEv++){
GenEvent(fv1, fv2);
if(iEv%10==0){
fv1->Print();
fv2->Print();
}
}
return 23;
}
// where the magin happens and we generate the decays of a massive particle
void GenEvent(FourVector *v1, FourVector *v2){
cout<<"Generating Event"<<endl;
// random generation
float mass = gen_mass(generator);
float esplit = gen_esplit(generator);
float p1_theta = gen_theta(generator);
float p1_phi = gen_phi(generator);
// translate to p1 assuming it is massless
float e1 = mass*esplit;
float pz1 = e1 * cos(p1_theta);
float py1 = e1 * sin(p1_theta) * sin(p1_phi);
float px1 = e1 * sin(p1_theta) * cos(p1_phi);
v1->SetFourVector(px1, py1, pz1, e1);
// conservation of momentum to get p2
FourVector *mother = new FourVector(0,0,0,mass);
v2->SetFourVector(mother->GetPx()-v1->GetPx(),
mother->GetPy()-v1->GetPy(),
mother->GetPz()-v1->GetPz(),
mother->GetE()-v1->GetE());
}
